TEMPLATE = app

QT += qml quick charts widgets

SOURCES += main.cpp

RESOURCES += calqlatr.qrc

OTHER_FILES = calqlatr.qml \
    content/*.js \
    content/*.qml
